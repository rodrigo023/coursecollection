﻿namespace Homework.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class MathUtilsTest
    {
        [TestMethod]
        public void Raiz9Es3()
        {
            Assert.AreEqual(3, MathUtils.SqRt(9));
        }

        [TestMethod]
        public void Raiz10000Es100()
        {
            Assert.AreEqual(100, MathUtils.SqRt(10000));
        }

        [TestMethod]
        public void RaizConDecimales()
        {
            Assert.AreEqual(76.39, MathUtils.SqRt(5836.369m));
        }

        [TestMethod]
        public void Sqrt8888Is94and52()
        {
            var input = 8888;

            Assert.AreEqual(94, 0);
            Assert.AreEqual(52, 0);
        }
    }
}