﻿namespace Homework
{
using System;
    using System.Linq;

    public class MathUtils
    {
        public static decimal SqRt(decimal number)
        {
            var sqRtTemp = 0.00m;
            var leftAuxLine = 0.00m;
            var rightAuxLine = 0.00m;
            var leftSide = GetLeftSide(number);
            var rightSide = GetRightSide(number);

            for (int i = 0; i < leftSide.ToString().Length; i += 2)
            {
                var numberPair = int.Parse(leftSide.ToString().Substring(i, 2));
                leftAuxLine = leftAuxLine == 0 ? numberPair : leftAuxLine * 100 + numberPair;
                if (leftAuxLine.ToString().Length > 2)
                {
                    var x = (Math.Floor(int.Parse(leftAuxLine.ToString().Substring(0, 2)) / (rightAuxLine * 2))).ToString();
                    rightAuxLine = int.Parse((rightAuxLine * 2).ToString() + x);
                    leftAuxLine = leftAuxLine - (rightAuxLine * int.Parse(x));
                    sqRtTemp = decimal.Parse(sqRtTemp.ToString() + x);
                }
                else
                {
                    var closestNumber = FindClosestNumber(leftAuxLine);
                    leftAuxLine = leftAuxLine - (closestNumber * closestNumber);
                    rightAuxLine = closestNumber;
                    sqRtTemp = rightAuxLine;
                }
            }

            for (int i = 0; i < rightSide.ToString().Length; i += 2)
            {
                var numberPair = int.Parse(rightSide.ToString().Substring(i, 2));
                leftAuxLine = leftAuxLine * 100 + numberPair;
                rightAuxLine = sqRtTemp * 2;
                var x = (Math.Floor(int.Parse(leftAuxLine.ToString().Substring(0, 3)) / (rightAuxLine))).ToString();
                rightAuxLine = rightAuxLine * 3;
               
            }

            return sqRtTemp;
        }

        private static int GetRightSide(decimal number)
        {
            var rightSide = int.Parse(number.ToString().Split(',')[1]);
            return rightSide.ToString().Length % 2 == 0 ? rightSide : rightSide * 10;
        }

        private static int GetLeftSide(decimal number)
        {
            return int.Parse(number.ToString().Split(',')[0]);
        }

        private static int FindClosestNumber(decimal number)
{
            var closestNumber = 0;
            foreach (var integer in Enumerable.Range(1, int.MaxValue))
    {
                var power = (decimal)Math.Pow(integer, 2);
                if (power > number)
        {
                    break;
                }

                closestNumber = integer;
            }
            return closestNumber;
        }
    }
}
