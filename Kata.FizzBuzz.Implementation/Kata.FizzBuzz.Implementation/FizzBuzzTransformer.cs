﻿namespace Kata.FizzBuzz.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class FizzBuzzTransformer : IFizzBuzzTransformer
    {
        public IEnumerable<string> Transform(IEnumerable<int> sequence)
        {
            var resultSequence = new List<string>();
            if (!sequence.Zip(sequence.Skip(1), (a, b) => (a + 1) == b).All(x => x))
            {
                throw new ArgumentException("No es secuencia");
            }

            foreach (var number in sequence)
            {
                if (number < 0)
                {
                    throw new ArgumentException("Negativo");
                }
                else if (number % 3 == 0 && number % 5 == 0)
                {
                    resultSequence.Add("FizzBuzz");
                }
                else if (number % 3 == 0)
                {
                    resultSequence.Add("Fizz");
                }
                else if (number % 5 == 0)
                {
                    resultSequence.Add("Buzz");
                }
                else
                {
                    resultSequence.Add(number.ToString());
                }
            }
            return resultSequence;
        }
    }
}
